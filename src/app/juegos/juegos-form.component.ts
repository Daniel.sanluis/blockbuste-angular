import { Component, OnInit } from '@angular/core';
import { Juego } from './juego';
import { JuegoService} from './juego.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Companias } from '../compania/companias'
import { CompaniasService } from '../compania/companias.service';

@Component({
  selector: 'app-juegos-form',
  templateUrl: './juegos-form.component.html',
  styleUrls: ['./juegos-form.component.css']
})
export class JuegosFormComponent implements OnInit {

  public juego: Juego = new Juego();
  public nuevoJuego : boolean;
  companias: Companias [] ;

  constructor(private juegoService: JuegoService,
     private companiasService: CompaniasService,
     private router: Router,
     private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.cargarJuego();
    this.companiasService.getCompanias().subscribe(companias => this.companias = companias)
  }

  public changedNuevoJuego() : void {
    this.nuevoJuego = !this.nuevoJuego;
  }

  cargarJuego(): void {
    this.activatedRoute.params.subscribe(params => {
      let titulo = params['titulo'];
      if(titulo){
        this.changedNuevoJuego();
        this.juegoService.getJuego(titulo).subscribe( (juego) => this.juego = juego
        )
      }
    })

  }

  compararComp(o1 : Companias, o2: Companias){

    return o1 === null || o2=== null? false: o1.nombre === o2.nombre;
  }
  
  public create(): void{
    this.juegoService.createJuego(this.juego).subscribe(
      response => this.router.navigate(['/juegos'])
    )
    console.log("Clicked")
    console.log(this.juego)
  }

  update(): void{
    this.juegoService.updateJuego(this.juego).subscribe(
      juego => this.router.navigate(['/juegos'])
    )
  }

}
