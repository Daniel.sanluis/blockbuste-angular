import { Companias } from '../compania/companias';

export class Juego {
    titulo: string;
    fechaLanzamiento: string;
    pegi: number;
    categoria: string;    
    companiaDto: Companias[];
}
