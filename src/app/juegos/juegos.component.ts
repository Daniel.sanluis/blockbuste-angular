import { Component, OnInit } from '@angular/core';
import { Juego } from './juego';
import  { JuegoService } from './juego.service';
import { AlertService } from '../alert/alert.service';
import { PageEvent } from '@angular/material/paginator';



@Component({
  selector: 'app-juegos',
  templateUrl: './juegos.component.html',
  styleUrls: ['./juegos.component.css']
})
export class JuegosComponent implements OnInit {

  juegos: Juego[];

  constructor(private juegoService: JuegoService,  private alertService: AlertService) { }

  ngOnInit(): void {
    this.refreshJuegos();
  }

  page_size: number = 5;
  page_number: number = 1;
  pageSizeOptions =[5,10,50,100]

  handlePage(e: PageEvent){
    this.page_size = e.pageSize;
    this.page_number = e.pageIndex + 1;
  }

  refreshJuegos(): void{
    this.juegoService.getJuegos().subscribe(
      juegos => this.juegos = juegos
    ) 
  }

  detete(titulo: string): void {    
    this.juegoService.deleteJuego(titulo).subscribe(
      response => {
      this.refreshJuegos();
    }
    )
  }

}
