import { Injectable } from '@angular/core';
import { Juego } from './juego';
import { catchError } from 'rxjs/operators'
import { Observable,of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders,  } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { AlertService } from '../alert/alert.service';


@Injectable({
  providedIn: 'root'
})
export class JuegoService {

  
  private urlEndPoint: string = 'http://localhost:8090/juego'

  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(private http: HttpClient, private alertService: AlertService) { }

  getJuegos(): Observable<Juego[]>{
    //return of(CLIENTES);
   return this.http.get(this.urlEndPoint).pipe(
      map( response => response as Juego[]),
      catchError(error => {
        console.error(`JuegoService::getJuegos error: "${error.message}"`);
        this.alertService.error(`Error al consultar los juegos: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);
      }));
  
  }

  createJuego(juego: Juego) : Observable<Juego> {
    this.alertService.success(`Creado el Juego: "${juego.titulo}" correctamente`, {autoClose: true, keepAfterRouteChange: true}); 
    return this.http.post<Juego>(this.urlEndPoint, juego, {headers: this.httpHeaders} ).pipe(
      catchError(error => {        
        console.error(`Juego::createJuego error: "${error.message}"`);
        this.alertService.error(`Error al crear el Juego: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);

      }));
  }  

  getJuego(titulo): Observable<Juego> {
    return this.http.get<Juego>(`${this.urlEndPoint}/${titulo}`).pipe(
      catchError(error => {        
        console.error(`Juego::getJuego error: "${error.message}"`);
        this.alertService.error(`No existe el Juego: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);

      }));
   // return this.http.get<Cliente>(this.urlEndPoint+'/'+dni)
  }

  updateJuego(juego : Juego) : Observable<Juego> {
    this.alertService.success(`Modificado el Juego: "${juego.titulo}" correctamente`, {autoClose: true, keepAfterRouteChange: true}); 
    return this.http.put<Juego>(`${this.urlEndPoint}/${juego.titulo}`, juego, {headers: this.httpHeaders}).pipe(
      catchError(error => {        
        console.error(`Juego::updateJuego error: "${error.message}"`);
        this.alertService.error(`El Juego no ha sido actualizado: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);

      }));    
  }

  deleteJuego(titulo): Observable<Juego>{
    this.alertService.success(`Eliminado el Juego correctamente`, {autoClose: true, keepAfterRouteChange: true}); 
    return this.http.delete<Juego>(`${this.urlEndPoint}/${titulo}`).pipe(
      catchError(error => {        
        console.error(`Juego::deleteJuego error: "${error.message}"`);
        this.alertService.error(`No se ha podido eliminar el Juego: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);

      }));
  }


}