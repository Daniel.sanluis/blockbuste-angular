import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MatPaginatorModule} from '@angular/material/paginator';
import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { ClientesComponent } from './clientes/clientes.component';
import { ClienteService } from './clientes/cliente.service';
import { HttpClientModule } from '@angular/common/http';
import {RouterModule, Routes } from '@angular/router';
import { JuegosComponent } from './juegos/juegos.component';
import { FormComponent } from './clientes/form.component';
import { FormsModule } from '@angular/forms';
import { JuegoService } from './juegos/juego.service';
import { CompaniaComponent } from './compania/compania.component';
import { CompaniasService } from './compania/companias.service';
import { StocksComponent } from './stocks/stocks.component';
import { StocksService } from './stocks/stocks.service';
import { TiendasComponent } from './tiendas/tiendas.component';
import { TiendasService } from './tiendas/tiendas.service';
import { AlertComponent } from './alert/alert.component';
import { RolesComponent } from './roles/roles.component';
import { JuegosFormComponent } from './juegos/juegos-form.component';
import { CompaniaFormComponent } from './compania/compania-form.component';
import { LoginComponent } from './login/login.component';
import { LoginService } from './login/login.service';
import { PaginatePipe } from './pipes/paginate.pipe';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TiendaFormComponent } from './tiendas/tienda-form.component';





const routes: Routes = [
  {path: '', redirectTo: '/clientes', pathMatch: 'full'},
  {path: 'clientes', component: ClientesComponent},
  {path: 'juegos', component: JuegosComponent},
  {path: 'compania', component: CompaniaComponent},
  {path: 'stocks', component: StocksComponent},
  {path: 'tiendas', component: TiendasComponent},
  {path: 'clientes/form', component: FormComponent},
  {path: 'clientes/form/:dni', component: FormComponent},
  {path: 'juegos/juegos-form', component: JuegosFormComponent},
  {path: 'juegos/juegos-form/:titulo', component: JuegosFormComponent},
  {path: 'compania/compania-form', component: CompaniaFormComponent},
  {path: 'compania/compania-form/:nombre', component: CompaniaFormComponent},
  {path: 'login', component: LoginComponent},
  {path: 'tiendas/tienda-form', component: TiendaFormComponent},
  {path: 'tiendas/tienda-form/:nombre', component: TiendaFormComponent}
  

]

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    ClientesComponent,
    JuegosComponent,
    FormComponent,
    CompaniaComponent,
    StocksComponent,
    TiendasComponent,
    AlertComponent,
    RolesComponent,
    JuegosFormComponent,
    CompaniaFormComponent,
    LoginComponent,
    PaginatePipe,
    TiendaFormComponent,
   
  
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes),
    BrowserAnimationsModule,
    MatPaginatorModule
  
    
  ],
  providers: [ClienteService, 
    JuegoService, 
    CompaniasService, 
    StocksService,
    TiendasService,
    LoginService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
