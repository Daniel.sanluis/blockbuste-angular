import { Juego } from '../juegos/juego';

export class Companias {
    cif: string;
    nombre: string;   
    juegos: Juego[];
}
