import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Companias } from '../compania/companias'
import { CompaniasService } from '../compania/companias.service';


@Component({
  selector: 'app-compania-form',
  templateUrl: './compania-form.component.html',
  styleUrls: ['./compania-form.component.css']
})
export class CompaniaFormComponent implements OnInit {

  public compania: Companias = new Companias();

  constructor(private companiasService: CompaniasService,
    private router: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    
  }

  public create(): void{
    this.companiasService.createCompania(this.compania).subscribe(
      response => this.router.navigate(['/compania'])
    )
    console.log("Clicked")
    console.log(this.compania)
  }

}
