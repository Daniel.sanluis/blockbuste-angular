import { Injectable } from '@angular/core';
import { Companias } from './companias';
import { map } from 'rxjs/operators';
import { Observable,of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators'
import { HttpClient, HttpHeaders,  } from '@angular/common/http';
import { AlertService } from '../alert/alert.service';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginService } from '../login/login.service'


@Injectable()
export class CompaniasService {
  
  private urlEndPoint: string = 'http://localhost:8090/companias'  

  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(private http: HttpClient,  private alertService: AlertService,
     private router: Router, private loginService: LoginService ) { }    
  

  getCompanias(): Observable<Companias[]>{
    //return of(CLIENTES);
    return this.http.get<Companias[]>(`${this.urlEndPoint}`, {headers: this.loginService.getAuthHeaders()}).pipe(
      map( response => response as Companias[]),
      catchError(error => {
        console.error(`ClienteService::getCompanias error: "${error.message}"`);
        if(error.status == 401){
          this.router.navigate(['/login'])
        }
        this.alertService.error(`Error al consultar las Compañias: "${error.message}"`, {autoClose: true, keepAfterRouteChange: false}); 
        return throwError(error);
      }));
  }

  createCompania(companias : Companias) : Observable<Companias> {
    this.alertService.success(`Creada la Compañia: "${companias.nombre}" correctamente`, {autoClose: true, keepAfterRouteChange: true}); 
    return this.http.post<Companias>(this.urlEndPoint + "/", companias, {headers: this.httpHeaders} ).pipe(
      catchError(error => {        
        console.error(`Juego::createtCompania error: "${error.message}"`);
        this.alertService.error(`Error al crear la Compañia: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);

      }));
  }  

  getCompania(nombre): Observable<Companias> {
    return this.http.get<Companias>(`${this.urlEndPoint}/${nombre}`).pipe(
      catchError(error => {        
        console.error(`Compañia::getCompania error: "${error.message}"`);
        this.alertService.error(`No existe la Compañia: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);

      }));
   // return this.http.get<Cliente>(this.urlEndPoint+'/'+dni)
  }

  deleteCompania(nombre): Observable<Companias>{
    this.alertService.success(`Eliminado la Compñia correctamente`, {autoClose: true, keepAfterRouteChange: true}); 
    return this.http.delete<Companias>(`${this.urlEndPoint}/${nombre}`).pipe(
      catchError(error => {        
        console.error(`Compañia::deleteCompania error: "${error.message}"`);
        this.alertService.error(`No se ha podido eliminar la compañia: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);

      }));
  }
}
