import { Component, OnInit } from '@angular/core';
import { CompaniasService } from './companias.service';
import { Companias} from './companias';
import { AlertService } from '../alert/alert.service';
import { LoginService } from '../login/login.service';
import { Router } from '@angular/router';
import { PageEvent } from '@angular/material/paginator';


@Component({
  selector: 'app-compania',
  templateUrl: './compania.component.html',
  styleUrls: ['./compania.component.css']
})
export class CompaniaComponent implements OnInit {

  token = localStorage.getItem('token')
 
  companias: Companias[];

  constructor(private companiasService: CompaniasService,  private alertService: AlertService, 
    private loginService: LoginService, private router: Router) { }

  ngOnInit(): void {
   this.refreshCompania();
  }

  page_size: number = 5;
  page_number: number = 1;
  pageSizeOptions =[5,10,50,100]

  handlePage(e: PageEvent){
    this.page_size = e.pageSize;
    this.page_number = e.pageIndex + 1;
  }

  logout(): void {
    this.loginService.deletetoken()
    this.router.navigate(['/login']);
    this.token = localStorage.getItem('token')
  } 

  refreshCompania(): void{
    this.companiasService.getCompanias().subscribe(
      companias => this.companias = companias
    ) 
  }

  detete(nombre: string): void {    
    this.companiasService.deleteCompania(nombre).subscribe(
      response => {
      this.refreshCompania();
    }
    )
  }


}
