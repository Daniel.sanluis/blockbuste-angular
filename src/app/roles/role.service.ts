import { Injectable } from '@angular/core';

import { catchError } from 'rxjs/operators'
import { Observable,of, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { AlertService } from '../alert/alert.service';
import { Role } from './role';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  private urlEndPoint: string = 'http://localhost:8090/roles'
  constructor(private http: HttpClient, private alertService: AlertService) { }


  getRoles(): Observable<Role[]> {
    return this.http.get(this.urlEndPoint).pipe(
      map( response => response as Role[]));
   // return this.http.get<Cliente>(this.urlEndPoint+'/'+dni)
  }
}
