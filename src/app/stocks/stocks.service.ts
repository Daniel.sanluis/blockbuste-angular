import { Injectable } from '@angular/core';
import { Stock } from './stock';
import { Observable,of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators'
import { HttpClient, HttpHeaders,  } from '@angular/common/http';
import { AlertService } from '../alert/alert.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StocksService {

   
  private urlEndPoint: string = 'http://localhost:8090/stock'

  constructor(private http: HttpClient, private alertService: AlertService) { }

  getStocks(): Observable<Stock[]>{
    //return of(CLIENTES);
    return this.http.get(this.urlEndPoint).pipe(
      map( response => response as Stock[]),
      catchError(error => {
        console.error(`ClienteService::getStocks: "${error.message}"`);
        this.alertService.error(`Error al consultar los Stocks: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);
      }));
  }

  deleteStock(referencia): Observable<Stock>{
    this.alertService.success(`Eliminado el Stock correctamente`, {autoClose: true, keepAfterRouteChange: true}); 
    return this.http.delete<Stock>(`${this.urlEndPoint}/${referencia}`).pipe(
      catchError(error => {        
        console.error(`Stock::deleteJuego error: "${error.message}"`);
        this.alertService.error(`No se ha podido eliminar el Stock: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);

      }));
  }

}
