import { Cliente } from '../clientes/cliente'
import { Juego } from '../juegos/juego'
import { Tienda } from '../tiendas/tienda'


export class Stock {
    cliente: Cliente;
    juegos: Juego;
    tiendas: Tienda;
    referencia: string;
    estado: string;
}
