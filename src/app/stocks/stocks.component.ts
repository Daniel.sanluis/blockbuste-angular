import { Component, OnInit } from '@angular/core';
import { Stock } from './stock';
import { StocksService  } from './stocks.service';
import { PageEvent } from '@angular/material/paginator';


@Component({
  selector: 'app-stocks',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.css']
})
export class StocksComponent implements OnInit {

  stocks: Stock[];

  constructor(private stocksService: StocksService) { }

  ngOnInit(): void {
    this.refreshStock();
  }

  page_size: number = 5;
  page_number: number = 1;
  pageSizeOptions =[5,10,50,100]

  handlePage(e: PageEvent){
    this.page_size = e.pageSize;
    this.page_number = e.pageIndex + 1;
  }

  refreshStock(): void{
    this.stocksService.getStocks().subscribe(
      stocks => this.stocks = stocks
    );
  }

  detete(referencia: number): void {    
    this.stocksService.deleteStock(referencia).subscribe(
      response => {
      this.refreshStock();
    }
    )
  }


}
