import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TiendasService } from './tiendas.service';
import { Tienda } from './tienda';


@Component({
  selector: 'app-tienda-form',
  templateUrl: './tienda-form.component.html',
  styleUrls: ['./tienda-form.component.css']
})
export class TiendaFormComponent implements OnInit {

  public tienda: Tienda = new Tienda();

  constructor(private tiendaService: TiendasService,
    private router: Router,
    private activatedRoute: ActivatedRoute) { }  

  ngOnInit(): void {
    
  }

  public create(): void{
    this.tiendaService.createTienda(this.tienda).subscribe(
      response => this.router.navigate(['/tiendas'])
    )
    console.log("Clicked")
    console.log(this.tienda)
  }

  
}

