import { Component, OnInit } from '@angular/core';
import { Tienda } from './tienda';
import { TiendasService } from './tiendas.service';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-tiendas',
  templateUrl: './tiendas.component.html',
  styleUrls: ['./tiendas.component.css']
})
export class TiendasComponent implements OnInit {

  tiendas: Tienda[];

  constructor(private tiendasService: TiendasService) { }

  ngOnInit(): void {
    this.refreshTienda();
  }

  page_size: number = 5;
  page_number: number = 1;
  pageSizeOptions =[5,10,50,100]

  handlePage(e: PageEvent){
    this.page_size = e.pageSize;
    this.page_number = e.pageIndex + 1;
  }

  refreshTienda(): void{
    this.tiendasService.getTiendas().subscribe(
      tiendas => this.tiendas = tiendas
    );
  }

  detete(nombre: string): void {    
    this.tiendasService.deleteTienda(nombre).subscribe(
      response => {
      this.refreshTienda();
    }
    )
  }

}
