import { Injectable } from '@angular/core';
import { Tienda } from './tienda';
import { Observable,of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators'
import { HttpClient, HttpHeaders,  } from '@angular/common/http';
import { AlertService } from '../alert/alert.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TiendasService {

  private urlEndPoint: string = 'http://localhost:8090/tienda'

  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(private http: HttpClient, private alertService: AlertService) { }

  getTiendas(): Observable<Tienda[]>{
    //return of(CLIENTES);
    return this.http.get(this.urlEndPoint).pipe(
      map( response => response as Tienda[]),
      catchError(error => {
        console.error(`ClienteService::getTiendas: "${error.message}"`);
        this.alertService.error(`Error al consultar las Tiendas: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);
      }));
  }

  createTienda(tienda : Tienda) : Observable<Tienda> {
    this.alertService.success(`Creada la Tienda: "${tienda.nombre}" correctamente`, {autoClose: true, keepAfterRouteChange: true}); 
    return this.http.post<Tienda>(this.urlEndPoint, tienda, {headers: this.httpHeaders} ).pipe(
      catchError(error => {        
        console.error(`Tienda::createTienda error: "${error.message}"`);
        this.alertService.error(`Error al crear la Tienda: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);

      }));
  }  

  deleteTienda(nombre): Observable<Tienda>{
    this.alertService.success(`Eliminado la Tienda correctamente`, {autoClose: true, keepAfterRouteChange: true}); 
    return this.http.delete<Tienda>(`${this.urlEndPoint}/${nombre}`).pipe(
      catchError(error => {        
        console.error(`Tienda::deleteTienda error: "${error.message}"`);
        this.alertService.error(`No se ha podido eliminar la Tienda: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);

      }));
  }



}
