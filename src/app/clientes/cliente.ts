import { Role } from '../roles/role';

export class Cliente  {
    nombre: string;
    userName: string;
    fechaNacimiento: string;
    correo: string;
    dni: string;
    pass: string;
    roles : Role[];
    
}

