import { Component, OnInit } from '@angular/core';
import { Cliente } from './cliente';
import { ClienteService } from './cliente.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Role } from '../roles/role';
import { RoleService } from '../roles/role.service'

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  public cliente: Cliente = new Cliente();
  public nuevoCliente : boolean;
  roles: Role [] ;
  constructor(private clienteService: ClienteService, 
    private router: Router,
     private activatedRoute: ActivatedRoute,
     private roleService: RoleService) { }

  ngOnInit(): void {
    this.cargarCliente();
    this.roleService.getRoles().subscribe(roles =>this.roles  = roles);

  }

  public changedNuevoCliente() : void {
    this.nuevoCliente = !this.nuevoCliente;
  }

  cargarCliente(): void {
    this.activatedRoute.params.subscribe(params => {
      let dni = params['dni'];
      if(dni){
        this.changedNuevoCliente();
        this.clienteService.getCliente(dni).subscribe( (cliente) => this.cliente = cliente
        )
      }
    })

  }

  compararRol(o1 : Role, o2: Role){

    return o1 === null || o2=== null? false: o1.rol === o2.rol;
  }
  
  public create(): void{
    this.clienteService.create(this.cliente).subscribe(
      response => this.router.navigate(['/clientes'])
    )
    console.log("Clicked")
    console.log(this.cliente)
  }

  update(): void{
    this.clienteService.update(this.cliente).subscribe(
      cliente => this.router.navigate(['/clientes'])
    )
  }

}
