import { Injectable } from '@angular/core';
import { Cliente } from './cliente';
import { CLIENTES} from './clientes.json';
import { Observable,of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators'
import { HttpClient, HttpHeaders,  } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { AlertService } from '../alert/alert.service';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  private urlEndPoint: string = 'http://localhost:8090/cliente';

  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'});

  

  constructor(private http: HttpClient, private alertService: AlertService) { }

  getClientes(): Observable<Cliente[]>{
    //return of(CLIENTES);
    return this.http.get(this.urlEndPoint).pipe(
      map( response => response as Cliente[]),
      catchError(error => {
        console.error(`ClienteService::getClientes error: "${error.message}"`);
        this.alertService.error(`Error al consultar los clientes: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);
      }));
      }

    
  create(cliente: Cliente) : Observable<Cliente> {
    this.alertService.success(`Creado el Cliente: "${cliente.nombre}" correctamente`, {autoClose: true, keepAfterRouteChange: true}); 
    return this.http.post<Cliente>(this.urlEndPoint, cliente, {headers: this.httpHeaders} ).pipe(
      catchError(error => {        
        console.error(`ClienteService::getClientes error: "${error.message}"`);
        this.alertService.error(`Error al crear el cliente: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);

      }));
  }  

  getCliente(dni): Observable<Cliente> {
    return this.http.get<Cliente>(`${this.urlEndPoint}/${dni}`).pipe(
      catchError(error => {        
        console.error(`ClienteService::getCliente error: "${error.message}"`);
        this.alertService.error(`No existe el cliente: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);

      }));
   // return this.http.get<Cliente>(this.urlEndPoint+'/'+dni)
  }

  update(cliente : Cliente) : Observable<Cliente> {
    this.alertService.success(`Modificado el Cliente: "${cliente.nombre}" correctamente`, {autoClose: true, keepAfterRouteChange: true}); 
    return this.http.put<Cliente>(`${this.urlEndPoint}/${cliente.dni}`, cliente, {headers: this.httpHeaders}).pipe(
      catchError(error => {        
        console.error(`ClienteService::updateCliente error: "${error.message}"`);
        this.alertService.error(`El cliente no ha sido actualizado: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);

      }));    
  }

  deleteCliente(dni): Observable<Cliente>{
    this.alertService.success(`Eliminado el Cliente correctamente`, {autoClose: true, keepAfterRouteChange: true}); 
    return this.http.delete<Cliente>(`${this.urlEndPoint}/${dni}`).pipe(
      catchError(error => {        
        console.error(`ClienteService::deleteCliente error: "${error.message}"`);
        this.alertService.error(`No se ha podido eliminar el cliente el cliente: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);

      }));
  }

}
