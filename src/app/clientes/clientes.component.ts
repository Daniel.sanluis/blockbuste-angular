import { Component, OnInit } from '@angular/core';
import { Cliente } from './cliente';
import { CLIENTES } from './clientes.json';
import { ClienteService } from './cliente.service';
import { AlertService } from '../alert/alert.service';
import { PageEvent } from '@angular/material/paginator';


@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css'],

})
export class ClientesComponent implements OnInit {

  clientes: Cliente[];


  constructor(private clienteService: ClienteService, private alertService: AlertService ) {
  
  }

  ngOnInit(): void {
   
    this.refreshClientes();
  }

  page_size: number = 5;
  page_number: number = 1;
  pageSizeOptions =[5,10,50,100]

  handlePage(e: PageEvent){
    this.page_size = e.pageSize;
    this.page_number = e.pageIndex + 1;
  }



  detete(dni: string): void {
    this.clienteService.deleteCliente(dni).subscribe(
      response => {
        this.refreshClientes();
      }
    )
  }

  refreshClientes(): void {
    this.clienteService.getClientes().subscribe(
      clientes => this.clientes = clientes
    )
  }

  
  }


